﻿
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour {

	public LoadSceneMode loadMode = LoadSceneMode.Single;	// Nos permite elegir carga normal o aditiva
	public bool asyncMode = false;							// Nos permite elegir carga síncrona o asíncrona

	public void ChangeScene ( int index ) {
		// En modo asíncrono cargamos la escena en segundo plano, dejando que el
		//  juego pueda seguir ejecutando normalmente durante el proceso de carga
		if ( asyncMode ) {
			StartCoroutine ( ChangeSceneCourutine ( index ) );
		}
		// En modo síncrono cargamos la escena normal, tarde lo que tarde en cargar
		else {
			SceneManager.LoadScene ( index , loadMode );
		}
			
	}

	// Corrutina de cambio de escena, las corrutinas admiten BUCLES (while) y gracias a la sentencia
	//  yield return null NO bloquean la ejecución, eso nos permite cargar la nueva escena sin apenas
	//  bloquear el juego, pudiendo realizar alguna animación básica durante el tiempo de carga
	public IEnumerator ChangeSceneCourutine ( int index ) {
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync ( index , loadMode );
		while (!asyncLoad.isDone ) {
			yield return null;
		}	
	}

}
