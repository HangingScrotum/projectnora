﻿using UnityEngine;
using System.Collections;


// The velocity in y is 10 units per second.  If the GameObject starts at (0,0,0) then
// it will reach (0,100,0) units after 10 seconds.

public class PlayerControl : MonoBehaviour
{
    public GameObject camera;
    public Rigidbody rbPlayer;
    public GameObject goPlayer;

    public float speed;
    public float ejeX,ejeZ;
    
    public bool groundCheck;
    public bool onAir = false;

    public bool activeControl;


    void Start()
    {       
        rbPlayer = GetComponent<Rigidbody>();
    }
    //Disposicion de PlayerControl
    public static PlayerControl main;
    void Awake ( ) {
		if ( main == null ) main = this;
	}

    void Update()
    {
        //movimiento
        if (activeControl==true){
        ejeZ=Input.GetAxis("Vertical");
        ejeX=Input.GetAxis("Horizontal");
        }
        
    }
    void FixedUpdate()
    {
        //Check de suelo
        if(Physics.Raycast(transform.position,Vector3.down,1.2f)) {
            //Suelo?        
            groundCheck = true;
            onAir=false;
            //Movimiento
            float velY=rbPlayer.velocity.y;
            rbPlayer.velocity = transform.forward * speed * ejeZ + transform.right * speed * ejeX + Vector3.up * velY;
            // rbPlayer.velocity=new Vector3((speed*ejeX),velY,(speed*ejeZ));
        }
        else{
            groundCheck= false;
            onAir=true;
        }
        //Salto
        if (Input.GetButtonDown("Jump")&&groundCheck==true&&activeControl==true)
        {
            
            rbPlayer.AddForceAtPosition(Vector3.up*5,rbPlayer.transform.position,ForceMode.Impulse);
            onAir = true;
            
        }
        //Giro con camara
        if(activeControl==true){
            Vector3 cameraForward = camera.transform.forward;
            cameraForward.y = 0;
            transform.forward = Vector3.Slerp ( transform.forward ,  cameraForward , 0.05f );
            
        }
        
    }
}