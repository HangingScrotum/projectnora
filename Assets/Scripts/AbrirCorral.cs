﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirCorral : MonoBehaviour {

	public Animator corral;
	public bool abierta;	
	public BoxCollider puerta;	


	
	// Update is called once per frame
	void Update () {
		if(puerta){
			if(Input.GetKeyDown("e")){
				corral.SetTrigger("Interactuar");
				abierta=!abierta;
				corral.SetBool("Abrir",abierta);
			}
		}
		
	}
}
