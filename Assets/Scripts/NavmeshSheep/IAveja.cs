﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAveja : MonoBehaviour {

	public GameObject target;
	public Animator ovimator;
	public NavMeshAgent navMeshveja;
	public enum State {Idle,Eat,Walk,Balar,Sit,IdleSit,SitUp,BalarSit};
	public State state=State.Idle;
	
	public float changeTime = 10f;	//Recalcular ruta
	public float range = 10f;
	public float countdown;
	public LayerMask meadowMask;	//Variables de control
	public bool sentada=false;
	public bool corralclose=true;
	private AbrirCorral scriptAbrilCorral;
	
	// Use this for initialization
	void Start () {
		target = Instantiate(target);
		target.transform.position= transform.position;
		navMeshveja= this.GetComponent<NavMeshAgent>();
		InvokeRepeating("RecalculateState",0,changeTime);
		scriptAbrilCorral = FindObjectOfType<AbrirCorral>( );
	}
	
	// Update is called once per frame
	void Update () {
				
		corralclose = !scriptAbrilCorral.abierta;
	}

	void RecalculateState(){
		if(sentada==true){
		countdown=0;
		while (countdown<10){
		 countdown+=Time.deltaTime;
		 //Debug.Log(countdown);		
		}			
		state=(State)Random.Range(5,8);
			if((int)state==6){sentada=false;}
			
		}
		else{
			state=(State)Random.Range(0,5);
			if ((int)state==4){sentada=true;}
			if(state==State.Walk){
				Vector3 nextTarget = transform.position + Vector3.up*10;
				Vector3 move;
				RaycastHit impact;
				// Tirada de rayo para buscar objetivo de movimiento dentro del establo o no
				if(corralclose==true){		
					move = Random.insideUnitSphere*range/2f;
					move.y = 1.8f;
					nextTarget += move;		
					Physics.Raycast ( nextTarget , Vector3.down , out impact);
					target.transform.position = impact.point;
					navMeshveja.SetDestination(target.transform.position);
					
				}			
					
				else {
					move = Random.insideUnitSphere*range*2f;
					move.y = 1.8f;
					nextTarget += move;	
					Physics.Raycast ( nextTarget , Vector3.down , out impact);
					target.transform.position = impact.point;	
					navMeshveja.SetDestination(target.transform.position);	
				}
				
			}
		}
		ovimator.SetInteger("State",(int)state);
		

		/* if(state==State.Idle) ovimator.SetInteger("State",0);
		if(state==State.Eat) ovimator.SetInteger("State",1);
		if(state==State.Walk) ovimator.SetInteger("State",2);
		if(state==State.Balar) ovimator.SetInteger("State",3);
		if(state==State.Sit) ovimator.SetInteger("State",(int)State.Sit);

		if(state==State.IdleSit) ovimator.SetInteger("State",5);
		if(state==State.SitUp) ovimator.SetInteger("State",6);		
		if(state==State.BalarSit) ovimator.SetInteger("State",7); */		
	}
}
