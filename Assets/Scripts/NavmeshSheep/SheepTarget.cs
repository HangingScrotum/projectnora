﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepTarget : MonoBehaviour {

	public LineRenderer line;

	// Update is called once per frame
	public void SetOrigin ( Vector3 pos ) {
		line.SetPosition ( 0 , transform.position );
		line.SetPosition ( 1 , pos );
	}
}
