﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Naima : NPC {

	public NavMeshAgent agent;
	public Collider actCM;
	public GameObject target;

	
	void Start()
	{
		myQuest=0;
	}

	public override void BehaviourDes ( ) { 
		if(QuestControl.main.activeQuest==1){
			state=NPC.State.Animated;
		}
		if(QuestControl.main.activeQuest>1){
			state=NPC.State.Invisible;
			gameObject.SetActive(false);
		}			

	}
	public override void BehaviourAct ( ) { 

		
		state = NPC.State.Animated;	
		

	}

}