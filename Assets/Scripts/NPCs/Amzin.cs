﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Amzin : NPC {

	public NavMeshAgent agent;
	public Collider actCM;
	public GameObject target;

	public GameObject nora;

	public bool cmStart,cmEnd;
	
	public static Amzin main;
    void Awake ( ) {
		if ( main == null ) main = this;
	}
	
	void Start()
	{		
		cmEnd=false;
		myQuest=1;
	}

	public override void BehaviourDes ( ) { 		
		state=NPC.State.Animated;

	}
	
	private void OnTriggerStay(Collider other) {
		if(other.tag=="Player"&&Input.GetKeyDown(KeyCode.E)){
			QuestControl.main.masterMontaje.SetTrigger("CM1AmzinE");
			cmStart=true;			
		}		
	}

	public override void BehaviourAct ( ) { 
		state=NPC.State.Animated;

		if(cmStart==true){
		state=NPC.State.Dialog;	
		PlayerControl.main.activeControl=false;
		PlayerControl.main.rbPlayer.velocity=new Vector3 (0f,0f,0f);
		PlayerControl.main.ejeX=0f;PlayerControl.main.ejeZ=0f;
		actCM.enabled=false;
		}

		
		if(cmEnd==true){
			state = NPC.State.Moving;
			target=nora;
			if(state==State.Moving){
			agent.SetDestination ( nora.transform.position );
			cmStart=false;
			}
		}
		
	}

}