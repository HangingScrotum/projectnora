﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class NPC : MonoBehaviour {

	public int myQuest;
	public enum State { Dialog , Moving , Animated , Invisible };
	public State state;

	public Animator animatorNPC;

	public KeyCode interactionKey = KeyCode.E;
	
	
	
	// Update is called once per frame
	void Update () {
		
		// COMPORTAMIENTO AUNQUE NO SEA SU QUEST LA ACTIVA
		// Aquí podemos gestionar estados aunque NO esté activa su quest (p. ej. diálogo)
		BehaviourDes();

		if ( QuestControl.main.activeQuest != myQuest ) return;
		Debug.Log ( "La entidad " + gameObject.name + " está disponible para la quest " + myQuest );

		// Aquí podemos gestionar estados que SÍ sean de su quest activa
		BehaviourAct ( );

	}

	void CompleteQuest ( ) {
		QuestControl.main.activeQuest++;
	}

	// Definimos que TODO NPC tendrá un método Behaviour, Abdel tendrá programado el suyo, etc
	public abstract void BehaviourAct ( );

	
	public abstract void BehaviourDes ( );

	}
