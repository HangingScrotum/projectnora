﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePause : MonoBehaviour {

	public bool pause;
	public GameObject panelMenu;
	public GameObject opciones;
	
	

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ( KeyCode.Escape )) {
			pause = !pause;
		}

		if ( pause ) {
			Time.timeScale = 0f;
			panelMenu.SetActive ( true );						
		}
		else {
			Time.timeScale = 1f;
			panelMenu.SetActive ( false );			
			if(opciones.active == true){
				opciones.SetActive ( false );
			}
		}

	}

	public void Continue ( ) {
		pause = false;
	}

}
