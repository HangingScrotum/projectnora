﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameCanvas : MonoBehaviour {
	
	public void GoToMenu () {
		SceneControl.main.ChangeScene ( 0 );	
	}

	public void GoToGame () {
		SceneControl.main.ChangeScene ( 1 );	
	}
}
