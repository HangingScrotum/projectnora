﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class QuestControl : MonoBehaviour {

	public int activeQuest = 0;
	public List<string> cinematicas;
	public List<GameObject> cinematicObjects;
	public CinemachineStateDrivenCamera cinemacosa;
	public Animator masterMontaje;

	public bool activeCM;
	
	//VariablesDialogos
	public GameObject dialogPanel;
	public List<string> dialogCM;
	public List<string> nameCM;
	public int ndialog;

	public KeyCode interactionKey = KeyCode.E;
	public KeyCode interactionKey2 = KeyCode.Mouse0;
	public KeyCode interactionKey3 = KeyCode.Insert;
	

	public float contador;
	


	//Script references
	private AbrirCorral scriptAbrilCorral;
	private Amzin scriptAmzin;
	//private PlayerControl scriptPlayerControl;



	//Disposicion de QuestControl
	public static QuestControl main;
	void Awake ( ) {
		if ( main == null ) main = this;
	}
	void Start() {
		activeQuest=0;
		scriptAbrilCorral = FindObjectOfType<AbrirCorral>( );
		scriptAmzin = FindObjectOfType<Amzin>( );
		//scriptPlayerControl = FindObjectOfType<PlayerControl>( );
		
		
		//cinemacosa.enabled = false;
		

	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log(activeQuest);
		if(activeQuest==0){			
			
			if(ndialog<4){
				PlayerControl.main.activeControl=false;
				//scriptPlayerControl.activeControl=false;
				CMNaima();
			}
			else{
				dialogPanel.SetActive ( false );				
				activeQuest++;
				masterMontaje.SetTrigger("FinCM");
				//scriptPlayerControl.activeControl=true;
				PlayerControl.main.activeControl=true;
			}		
		}
		if(activeQuest==1){
						
			if(scriptAmzin.cmStart==true){				
				if(ndialog==4){
					
					dialogPanel.SetActive(true);
					GameObject panelName = GameObject.FindGameObjectWithTag("PanelName");
					Text nameText = panelName.GetComponentInChildren<Text>( );
					nameText.text = nameCM[ndialog];

					GameObject panelText = GameObject.FindGameObjectWithTag("PanelText");
					Text dialogText = panelText.GetComponentInChildren<Text>( );
					dialogText.text = dialogCM[ndialog];
					contador=0f;
					contador=contador+Time.deltaTime;
					if ( contador>=8f|| Input.GetKeyDown (interactionKey2)||Input.GetKeyDown (interactionKey3)) {				
					ActDialog();					
					}
				}
				if(ndialog>=5 && ndialog<=8){				
				CMAmzin();
				}
				if (ndialog==9){
					
					contador=0f;
					contador=contador+Time.deltaTime;
					if ( contador>=8f||Input.GetKeyDown (interactionKey)|| Input.GetKeyDown (interactionKey2)||Input.GetKeyDown (interactionKey3)) {	
					Amzin.main.cmEnd=true;
					masterMontaje.SetTrigger("FinCM");
					}
				}
				//Debug.Log("AllahuAkbar");
			}
			if(scriptAmzin.cmEnd==true){
				dialogPanel.SetActive(false);
				PlayerControl.main.activeControl=true;
			}
			
			

			
		}
		//if ( Input.GetKeyDown ( KeyCode.F10))  cinematicObjects [ 0 ].SetActive ( true );
		//if ( Input.GetKeyDown ( KeyCode.F1))  cinematicObjects [ 1 ].SetActive ( true );
		//if ( Input.GetKeyDown ( KeyCode.F2))  cinematicObjects [ 2 ].SetActive ( true );
	}
	public void CMNaima(){
		
			contador=contador+Time.deltaTime;
			
			//Debug.Log(contador);
			dialogPanel.SetActive ( true );
			//Cargado de primer dialogo
			if(ndialog==0){
				GameObject panelName = GameObject.FindGameObjectWithTag("PanelName");
				Text nameText = panelName.GetComponentInChildren<Text>( );
				nameText.text = nameCM[ndialog];

				GameObject panelText = GameObject.FindGameObjectWithTag("PanelText");
				Text dialogText = panelText.GetComponentInChildren<Text>( );
				dialogText.text = dialogCM[ndialog];				
			}
			if ( contador>=8f||Input.GetKeyDown (interactionKey)|| Input.GetKeyDown (interactionKey2)||Input.GetKeyDown (interactionKey3)) {				
				ActDialog();				
			}
			
		
	}

	public void CMAmzin(){
		contador=contador+Time.deltaTime;
		
		//Debug.Log(contador);
		dialogPanel.SetActive ( true );		
		if ( contador>=8f||Input.GetKeyDown (interactionKey)|| Input.GetKeyDown (interactionKey2)||Input.GetKeyDown (interactionKey3)) {				
				ActDialog();
			}

	}
	//Actualizar Dialogos
	public void ActDialog(){
		//Avance de numero de dialogo y quien habla
		ndialog++;
		//trigger para cambio camara CM
		masterMontaje.SetTrigger("Accion");

		GameObject panelName = GameObject.FindGameObjectWithTag("PanelName");
		Text nameText = panelName.GetComponentInChildren<Text>( );
		nameText.text = nameCM[ndialog];

		GameObject panelText = GameObject.FindGameObjectWithTag("PanelText");
		Text dialogText = panelText.GetComponentInChildren<Text>( );
		dialogText.text = dialogCM[ndialog];
		contador=0f;
		
	}
 
}

	
